const Footer = ({ className }) => {
  return (
    <>
      <footer>
        <div>
          <h3>Think People</h3>
          <p>
            We are a passionate group of professionals that fulfill your
            recruiting requirements while ensuring exceptional quality and help
            you find the candidate with the right skills and talent.
          </p>
        </div>
        <div>
          <h3>Connect With Us Now</h3>
          <p>We are an online agency. </p>
          <p>Email: info@thinkpeople.in</p>
          <p>Phone: +91 99239 37232</p>
        </div>
        <div>
          <h3>Find Us On Social Media</h3>
          <p>Linkedin</p>
          <p>Facebook</p>
          <p>Instagram</p>
        </div>
      </footer>
      <style jsx>{`
        footer {
          width: 60%;
          border-top: 1px solid #eaeaea;
          display: flex;
          justify-content: center;
          align-items: center;
          margin: auto;
        }
        footer div {
          width: 33%;
          padding: 24px;
        }
        h3 {
          color: #ce434a;
        }
      `}</style>
    </>
  );
};

export default Footer;
