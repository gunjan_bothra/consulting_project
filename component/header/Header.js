import Link from "next/link";

import Image from "next/image";

const Header = () => {
  return (
    <header className="header">
      <div className="image">
        <Image
          src="/images/Space.png" // Route of the image file
          height={100} // Desired size with correct aspect ratio
          width={100} // Desired size with correct aspect ratio
          alt="About Company"
        />
      </div>
      <nav>
        <ul>
          <li>
            <Link href="/">
              <a>Home</a>
            </Link>
          </li>
          <li>
            <Link href="/about">
              <a>About</a>
            </Link>
          </li>
          <li>
            <Link href="/services">
              <a>Services</a>
            </Link>
          </li>
          <li>
            <Link href="/contact">
              <a>Contact</a>
            </Link>
          </li>
        </ul>
      </nav>

      <style jsx>{`
        .header {
          display: flex;
          width: 60%;
          margin: auto;
          margin-top: 40px;
        }
        .image {
          flex-grow: 1;
        }
        ul {
          display: flex;
          list-style: none;
          width: 609px;
          text-align: center;
        }
        li {
          flex-grow: 1;
        }
      `}</style>
    </header>
  );
};

export default Header;
