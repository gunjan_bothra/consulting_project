const servicesData = [
  {
    image: "/images/erp.jpeg",
    title: "ERP",
    description:
      "With a strong penchant for ERP, be assured to get the right solution for your requirements.",
  },
  {
    image: "/images/crm.jpeg",
    title: "CRM",
    description:
      "With a strong exposure to various industry verticals, we are well equipped to provide the apt CRM solutions for your organization.",
  },
  {
    image: "/images/itsm.jpeg",
    title: "ITSM",
    description:
      "From IT Service Management to Operation Management, we got you all covered!",
  },
  {
    image: "/images/computing.jpeg",
    title: "Cloud Computing",
    description:
      "Our cloud experts are here to help you keep pace with the ever-evolving technological space.",
  },
  {
    image: "/images/devops.jpeg",
    title: "DevOps",
    description:
      "We understand the wide requirements of DevOps and are ready with all the tools and solutions to aid your organization’s growth.",
  },
  {
    image: "/images/data_science.jpeg",
    title: "Data Science",
    description:
      "Looking to drive the right insights from your abundant data? Look no further, you are at the right platform.",
  },
];

export { servicesData };
