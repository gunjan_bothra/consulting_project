import Head from "next/head";
import Header from "../component/header/Header";
import Footer from "../component/footer/Footer";

const About = () => {
  return (
    <>
      <Head>
        <title>About</title>
      </Head>
      <Header></Header>
      <main>
        <h1>What we do</h1>
        <div>
          <p>
            Technology plays a pivotal role in the overall performance of an
            organization. Successful technology implementation ensures improved
            productivity through automation and process definition. An
            organization needs expert IT consulting to achieve successful
            implementation. IntelligenzIT was thus born to bridge this gap. With
            close to two decades of expertise, our team caters to a plethora of
            domains like Supply Chain, E-commerce, Automobile, Telecom, Oil &
            Gas, BFSI, Technology and many more. In addition to this, we also
            focus on SAP, Salesforce, Servicenow, AWS, Azure, GCP, DevOps and
            Data Science amongst several other niche technologies.
          </p>
          <p>
            In a strive to give back more to the industry and create a positive
            impact, we have been able to build a network of clientele that
            include Cognizant, Sopra & KPS, to name a few.{" "}
          </p>
          <p>Looking for a digital transformation? Say no more.</p>
        </div>
      </main>
      <div className="footer-content">
        <Footer></Footer>
      </div>
      <style jsx>{`
        main {
          text-align: center;
          font-size: 18px;
          font-family: sans-serif;
          width: 60%;
          margin: auto;
          margin-top: 40px;
          font-size: 20px;
        }
        h1 {
          color: #ce434a;
        }
        .footer-content {
          position: fixed !important;
          bottom: 0;
        }
        p {
          line-height: 32px;
        }
      `}</style>
    </>
  );
};

export default About;
