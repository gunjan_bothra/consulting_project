import Head from "next/head";
import Header from "../component/header/Header";
import Footer from "../component/footer/Footer";

const Contact = () => {
  return (
    <>
      <Head>
        <title>Contact</title>
      </Head>
      <Header></Header>
      <h1>Contact</h1>
      <Footer></Footer>
    </>
  );
};

export default Contact;
