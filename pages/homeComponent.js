import Link from "next/link";
import Image from "next/image";
import { servicesData } from "../mock/services";

const HomeComponent = () => {
  return (
    <div>
      {servicesData.map((service, index) => {
        <Link href="/services" key={index}>
          <a>
            <Image
              src={service.image} // Route of the image file
              height={250} // Desired size with correct aspect ratio
              width={250} // Desired size with correct aspect ratio
              alt={service.title}
            />
          </a>
        </Link>;
      })}
    </div>
  );
};

export default HomeComponent;
