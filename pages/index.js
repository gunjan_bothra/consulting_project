import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
import Header from "../component/header/Header";
import Footer from "../component/footer/Footer";
import { servicesData } from "../mock/services";
// import HomeComponent from "./homeComponent";

export default function Home({ servicesData }) {
  console.log(servicesData);
  return (
    <div className="container">
      {/* <Head>
        <title>Home</title>
        <link rel="icon" href="/favicon.ico" />
      </Head> */}

      {/* <Header></Header>
      <header>
        <Image
          src="/images/services-banner.jpeg" // Route of the image file
          height={500} // Desired size with correct aspect ratio
          width={2500} // Desired size with correct aspect ratio
          alt="About Company"
        />
      </header>
       */}
      {/* <main>
        <h1>Our Services</h1>
        <div>
          <Link href="/services">
            <a>
              <Image
                src="/images/Home.jpeg" // Route of the image file
                height={250} // Desired size with correct aspect ratio
                width={250} // Desired size with correct aspect ratio
                alt="CRM"
              />
            </a>
          </Link>
          <Link href="/services">
            <a>
              <Image
                src="/images/Home.jpeg" // Route of the image file
                height={250} // Desired size with correct aspect ratio
                width={250} // Desired size with correct aspect ratio
                alt="ERP"
              />
            </a>
          </Link>
          <Link href="/services">
            <a>
              <Image
                src="/images/Home.jpeg" // Route of the image file
                height={250} // Desired size with correct aspect ratio
                width={250} // Desired size with correct aspect ratio
                alt="Cloud Computing"
              />
            </a>
          </Link>
        </div>
        <div>
          <Link href="/services">
            <a>
              <h2>CRM</h2>
            </a>
          </Link>
          <Link href="/services">
            <a>
              <h2>ERP</h2>
            </a>
          </Link>
          <Link href="/services">
            <a>
              <h2>Cloud Computing</h2>
            </a>
          </Link>
        </div>
      </main> */}

      {/* <main>
        <h1>Our Services</h1>
        <div>
          {servicesData.map((service, index) => {
            <div>
              <Link href="/services" key={index}>
                <a>
                  <Image
                    src={service.image} // Route of the image file
                    height={250} // Desired size with correct aspect ratio
                    width={250} // Desired size with correct aspect ratio
                    alt={service.title}
                  />
                </a>
                ;
              </Link>
              ;
            </div>;
          })}
        </div>
      </main> */}

      <Head>
        <title>Home</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header></Header>
      <header>
        <Image
          src="/images/services-banner.jpeg" // Route of the image file
          height={500} // Desired size with correct aspect ratio
          width={2500} // Desired size with correct aspect ratio
          alt="About Company"
        />
      </header>

      <main>
        <h1>Our Services</h1>
        <ul>
          {servicesData.map((service, index) => (
            <li>
              <a>
                <Image
                  src={service.image} // Route of the image file
                  height={250} // Desired size with correct aspect ratio
                  width={250} // Desired size with correct aspect ratio
                  alt={service.title}
                />
              </a>
              <h3>{service.title}</h3>
            </li>
          ))}
        </ul>
      </main>
      <Footer></Footer>
      <style jsx>{`
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }
        main {
          padding: 5rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          width: 80%;
          margin: auto;
        }
        header {
          margin-top: 48px;
        }
        main div {
          display: flex;
          width: 80%;
        }
        main div a {
          width: 33%;
        }

        a {
          color: inherit;
          text-decoration: none;
        }

        .logo {
          height: 1em;
        }
        ul {
          display: flex;
          list-style: none;
          flex-wrap: wrap;
          align-items: center;
          justify-content: center;
        }
        li {
          flex: 33%;
          margin-bottom: 80px;
        }
        h3 {
          color: #ce434a;
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }
        h1 {
          color: #ce434a;
        }
        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  );
}

export async function getStaticProps() {
  return {
    props: {
      servicesData,
    },
  };
}
