import Head from "next/head";
import Image from "next/image";
import Header from "../component/header/Header";
import Footer from "../component/footer/Footer";
import { servicesData } from "../mock/services";

const Services = () => {
  return (
    <>
      <Head>
        <title>Services</title>
      </Head>
      <Header></Header>
      <main>
        <h1 className="heading">What We Offer</h1>
        {servicesData.map((service, index) => {
          return index % 2 === 0 ? (
            <section>
              <div>
                <Image
                  src={service.image} // Route of the image file
                  height={400} // Desired size with correct aspect ratio
                  width={700} // Desired size with correct aspect ratio
                  alt="About Company"
                />
              </div>
              <div className="desc-outer desc-left">
                <h2>{service.title}</h2>
                <div className="desc">
                  <p>{service.description}</p>
                </div>
              </div>
            </section>
          ) : (
            <section>
              <div className="desc-outer desc-right">
                <h2>{service.title}</h2>
                <div className="desc">
                  <p>{service.description}</p>
                </div>
              </div>
              <div>
                <Image
                  src={service.image} // Route of the image file
                  height={400} // Desired size with correct aspect ratio
                  width={700} // Desired size with correct aspect ratio
                  alt="About Company"
                />
              </div>
            </section>
          );
        })}
      </main>
      <Footer></Footer>
      <style jsx>{`
        main {
          width: 80%;
          margin: auto;
          margin-top: 40px;
        }
        p {
          line-height: 1.75em;
          text-align: center;
          font-size: 20px;
        }
        .heading {
          color: #ce434a;
          text-align: center;
        }
        section {
          display: flex;
          width: 100%;
          margin-bottom: 20px;
        }
        section div {
          width: 50%;
          text-align: center;
        }
        .desc {
          margin-left: 20%;
          margin-right: 20%;
        }
        .desc-outer {
          background-color: #fdecdcd1;
          align-items: center;
          justify-content: center;
          display: flex;
          flex-direction: column;
        }
        .desc-left {
          margin-left: 24px;
        }
        .desc-right {
          margin-right: 24px;
        }
        h2 {
          color: #ce434a;
        }
      `}</style>
    </>
  );
};

export default Services;
